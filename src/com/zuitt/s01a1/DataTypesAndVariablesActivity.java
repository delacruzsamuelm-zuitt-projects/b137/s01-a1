package com.zuitt.s01a1;

public class DataTypesAndVariablesActivity {
    public static void main(String[] args) {
        String firstName = "Sam";
        String lastName ="Dela Cruz";
        double englishGrade = 90.25;
        double mathGrade = 91.25;
        double scienceGrade = 92.25;
        double averageGrade = (englishGrade + mathGrade + scienceGrade) / 3;

        System.out.println("Name: " + firstName + " " + lastName + " and your average is: " + averageGrade);

    }
}
